package rest;
	
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.w3c.dom.ls.LSInput;

import com.jayway.restassured.internal.http.Status;

import domain.Film;

import domain.Comment;
import domain.services.FilmService;
	
	@Path("/films")
	@Stateless
	public class FilmResources {
	
		private FilmService db = new FilmService();
		
		@PersistenceContext
		EntityManager em;
		
		@GET
		@Produces({MediaType.APPLICATION_JSON})
		public List<Film> getAll()
		{
			return em.createNamedQuery("film.all",Film.class).getResultList();
		}
		
		@POST
		@Consumes({MediaType.APPLICATION_JSON})
		public Response Add(Film film){
			em.persist(film);
			return Response.ok(film.getId()).build();
		}
		
		@GET
		@Path("/{id}")
		@Produces({MediaType.APPLICATION_JSON})
		public Response get(@PathParam("id") int id){
			Film result = em.createNamedQuery("film.id", Film.class)
					.setParameter("filmId", id)
					.getSingleResult();
			if(result==null){
				return Response.status(404).build();
			}
			return Response.ok(result).build();
		}
		
		@PUT
		@Path("/{id}")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response update(@PathParam("id") int id, Film f){
			Film result = em.createNamedQuery("film.id", Film.class)
					.setParameter("filmId", id)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
			result.setTitle(f.getTitle());
			result.setYear(f.getYear());
			em.persist(result);
			return Response.ok().build();
		}
		
		
		@GET
		@Path("/{id}/comments")
		@Produces({MediaType.APPLICATION_JSON})
		public List<Comment> getComments(@PathParam("filmId") int id){
			Film result = em.createNamedQuery("film.id", Film.class)
					.setParameter("filmId", id)
					.getSingleResult();
			if(result==null)
				return null;
			//if(result.getComments()==null)
			//	result.setComments(new ArrayList<Comment>());
			return result.getComments();
		}
		
		@POST
		@Path("/{id}/comments")
		@Consumes({MediaType.APPLICATION_JSON})
		public Response addComment(@PathParam("id") int id, Comment comment){
			Film result = em.createNamedQuery("film.id", Film.class)
					.setParameter("filmId", id)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
			//if(result.getComments()==null)
			//	result.setComments(new ArrayList<Comment>());
			result.getComments().add(comment);
			comment.setFilm(result);
			em.persist(comment);
			return Response.ok().build();
		}
		
		@DELETE
		@Path("/{id}/comments")
		public Response delete(@PathParam("id") int commentId){
			Film result = em.createNamedQuery("film.id", Film.class)
					.setParameter("commentId", commentId)
					.getSingleResult();
			if(result==null)
				return Response.status(404).build();
			em.remove(result);
			return Response.ok().build();
		}
		
		
	}
	
	
	
	

	
